/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ImageKnifeComponent, ImageKnifeOption } from "@ohos/libraryimageknife"
import matrix4 from '@ohos.matrix4'

@Entry
@ComponentV2
struct LoadStatePage {

  starTime:number = new Date().getTime()

  @Local ImageKnifeOption: ImageKnifeOption = new ImageKnifeOption({
    loadSrc: $r("app.media.rabbit"),
    placeholderSrc: $r("app.media.loading"),
    errorholderSrc: $r("app.media.app_icon"),
    objectFit: ImageFit.Contain,
    onLoadListener: {
      onLoadFailed: (err) => {
        console.error("Load Failed Reason: " + err);
      },
      onLoadSuccess: (data) => {
        return data;
      },
    },
    border: { radius: 50 }
  })
  @Local imageKnifeOption1: ImageKnifeOption = new ImageKnifeOption({
    loadSrc: $r('app.media.startIcon')
  })
  @Local message: string = ""
  @Local currentWidth: number = 200
  @Local currentHeight: number = 200
  @Local typeValue: string = ""
  build() {
    Column() {
      Text($r('app.string.TIPS'))
        .margin({ top: 20 })
      Row() {
        Button($r('app.string.Test_failure_success'))
          .onClick(() => {
            this.ImageKnifeOption = new ImageKnifeOption({
              loadSrc: "https://www.openharmony.cn/_nuxt/img/logo.dcf95b3.png",
              placeholderSrc: $r("app.media.loading"),
              errorholderSrc: $r("app.media.app_icon"),
              objectFit: ImageFit.Contain,
              onLoadListener: {
                onLoadStart:()=>{
                  this.starTime = new Date().getTime()
                  console.info("Load start: ");
                },
                onLoadFailed: (err) => {
                  console.error("Load Failed Reason: " + err + "  cost " + (new Date().getTime() - this.starTime) + " milliseconds");
                },
                onLoadSuccess: (data,imageData) => {
                  console.info("Load Successful: cost " + (new Date().getTime() - this.starTime) + " milliseconds");
                  this.currentWidth = imageData.imageWidth!
                  this.currentHeight = imageData.imageHeight!
                  this.typeValue = imageData.type!
                  return data;
                },
              },
              border: { radius: 50 },
              onComplete:(event)=>{
                console.error("Load onComplete width:"+event?.width , " height:"+event?.height , " componentWidth:"+event?.componentWidth," componentHeight:" + event?.componentHeight);
              }
            })
          })
      }
      .margin({ top: 20 })
      Text($r('app.string.image_format',this.typeValue))
      Text($r('app.string.image_width',this.currentWidth))
      Text($r('app.string.image_height',this.currentHeight))
      ImageKnifeComponent({ imageKnifeOption: this.ImageKnifeOption }).height(this.currentHeight).width(this.currentWidth)
        .margin({ top: 20 })
      Button($r('app.string.Custom_download_failed')).onClick(()=>{
        this.imageKnifeOption1 = new ImageKnifeOption({
          loadSrc: "abc",
          placeholderSrc:$r('app.media.loading'),
          errorholderSrc:$r('app.media.failed'),
          customGetImage:custom,
          onLoadListener: {
            onLoadFailed:(err)=>{
              this.message = "err:" + err
            }
          }
        })
      }).margin({ top: 20 })
      Text(this.message).fontSize(20).margin({ top: 20 })
      ImageKnifeComponent({ imageKnifeOption: this.imageKnifeOption1 }).height(this.currentHeight).width(this.currentWidth)
        .margin({ top: 20 })
    }

    .width('100%')
    .height('100%')
  }

}
// 自定义下载方法
@Concurrent
async function custom(context: Context, src: string | PixelMap | Resource): Promise<ArrayBuffer | undefined> {
  console.info("ImageKnife::  custom download：" + src)
  // 举例写死从本地文件读取，也可以自己请求网络图片
  return undefined
}