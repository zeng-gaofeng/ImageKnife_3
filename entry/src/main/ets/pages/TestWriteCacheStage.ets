/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ImageKnifeComponent,CacheStrategy,ImageKnifeOption } from '@ohos/libraryimageknife'

@Entry
@ComponentV2
struct TestWriteCacheStage {
  @Local imageKnifeOption1: ImageKnifeOption = new ImageKnifeOption({
    loadSrc:$r('app.media.startIcon'),
    placeholderSrc:$r('app.media.loading'),
    errorholderSrc:$r('app.media.failed')
  })
  @Local imageKnifeOption2: ImageKnifeOption = new ImageKnifeOption({
    loadSrc:$r('app.media.startIcon'),
    placeholderSrc:$r('app.media.loading'),
    errorholderSrc:$r('app.media.failed')
  })
  @Local imageKnifeOption3: ImageKnifeOption = new ImageKnifeOption({
    loadSrc:$r('app.media.startIcon'),
    placeholderSrc:$r('app.media.loading'),
    errorholderSrc:$r('app.media.failed')
  })

  build() {
    Column() {
      Button($r('app.string.Write_memory_and_file')).margin({top:10}).onClick(async ()=>{
        this.imageKnifeOption1 = new ImageKnifeOption({
          loadSrc:'https://hbimg.huabanimg.com/95a6d37a39aa0b70d48fa18dc7df8309e2e0e8e85571e-x4hhks_fw658/format/webp',
          placeholderSrc:$r('app.media.loading'),
          errorholderSrc:$r('app.media.failed'),
          writeCacheStrategy:CacheStrategy.Default
        })
      })
      ImageKnifeComponent({
        imageKnifeOption: this.imageKnifeOption1
      }).width(200).height(200).margin({top:10})
      Button($r('app.string.Write_memory')).margin({top:10}).onClick(async ()=>{
        this.imageKnifeOption2 = new ImageKnifeOption({
          loadSrc:"https://hbimg.huabanimg.com/cc6af25f8d782d3cf3122bef4e61571378271145735e9-vEVggB",
          placeholderSrc:$r('app.media.loading'),
          errorholderSrc:$r('app.media.failed'),
          writeCacheStrategy:CacheStrategy.Memory
        })
      })
      ImageKnifeComponent({
        imageKnifeOption: this.imageKnifeOption2
      }).width(200).height(200).margin({top:10})
      Button($r('app.string.Write_file')).margin({top:10}).onClick(async ()=>{
        this.imageKnifeOption3 = new ImageKnifeOption({
          loadSrc:'https://img-blog.csdn.net/20140514114029140',
          placeholderSrc:$r('app.media.loading'),
          errorholderSrc:$r('app.media.failed'),
          writeCacheStrategy:CacheStrategy.File
        })
      })
      ImageKnifeComponent({
        imageKnifeOption: this.imageKnifeOption3
      }).width(200).height(200).margin({top:10})
    }
    .height('100%') .width('100%')
  }
}