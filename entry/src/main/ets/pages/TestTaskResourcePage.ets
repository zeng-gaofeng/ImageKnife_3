/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ImageKnifeComponent, ImageKnifeOption } from "@ohos/libraryimageknife"

@ComponentV2
export struct ZuImage {
  @Param @Require src: PixelMap | ResourceStr | string | undefined
  @Param @Require placeholderSrc: PixelMap | ResourceStr | string | undefined
  @Local errorholderSrc: PixelMap | ResourceStr | string | undefined

  build() {
    if (this.src) {
      //当前版本存在bug
      ImageKnifeComponent({
        imageKnifeOption: new ImageKnifeOption({
          loadSrc: this.src,
          placeholderSrc: this.placeholderSrc,
          errorholderSrc: this.errorholderSrc ?? this.placeholderSrc,
          objectFit: ImageFit.Cover
        })
      })
    } else {
      Image(this.placeholderSrc)
        .objectFit(ImageFit.Cover)
    }
  }
}


@Entry
@ComponentV2
struct TestTaskResourcePage {
  @Local stateMenus: Array<string> = [
    "https://hbimg.huabanimg.com/cc6af25f8d782d3cf3122bef4e61571378271145735e9-vEVggB",
    'https://contentcenter-drcn.dbankcdn.cn/pub_1/DevEcoSpace_1_900_9/bf/v3/lSjrRwFcS-ez6jp1ALSQFg/0n7R7XinSPyrYLqDu_1dfw.jpg',
    'https://img-blog.csdn.net/20140514114029140',
    'https://hbimg.huabanimg.com/95a6d37a39aa0b70d48fa18dc7df8309e2e0e8e85571e-x4hhks_fw658/format/webp',
  ]
  @Builder
  _buildItem(item: string) {
    Column({ space: 8 }) {
      ZuImage({
        src: item,
        placeholderSrc: $r("app.media.loading")
      }).width(44)
        .height(44)
    }
  }
  build() {

    Grid() {
      ForEach(this.stateMenus, (item: string) => {
        GridItem() {
          this._buildItem(item)
        }
      })
    }.width("100%")
    .columnsTemplate('1fr 1fr 1fr 1fr 1fr')
    .rowsGap(24)
    .padding({
      top: 24,
      bottom: 24,
      left: 24,
      right: 24
    })
  }
}