/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from '@ohos/hypium'
import { image } from '@kit.ImageKit'
import { BusinessError } from '@kit.BasicServicesKit'
import { calculateScaleType, Downsampler } from '@ohos/imageknife/src/main/ets/downsampling/Downsampler'
import { DownsampleStrategy } from '@ohos/imageknife'

export default function SamplingTest() {
  describe('SamplingTest', () => {
    // Defines a test suite. Two parameters are supported: test suite name and test suite function.
    beforeAll(() => {
      // Presets an action, which is performed only once before all test cases of the test suite start.
      // This API supports only one parameter: preset action function.
    })
    beforeEach(() => {
      // Presets an action, which is performed before each unit test case starts.
      // The number of execution times is the same as the number of test cases defined by **it**.
      // This API supports only one parameter: preset action function.
    })
    afterEach(() => {
      // Presets a clear action, which is performed after each unit test case ends.
      // The number of execution times is the same as the number of test cases defined by **it**.
      // This API supports only one parameter: clear action function.
    })
    afterAll(() => {
      // Presets a clear action, which is performed after all test cases of the test suite end.
      // This API supports only one parameter: clear action function.
    })
    it('NONE', 0, () => {
      let reqSize: calculateScaleType =
        new Downsampler().calculateScaling('jpg', 1024, 1024, 200,
          200, DownsampleStrategy.NONE,)
      let req = (reqSize.targetWidth == 1024 && reqSize.targetHeight == 1024)
      expect(req).assertEqual(true);
    })
    it('AT_MOST', 1, () => {
      let reqSize: calculateScaleType =
        new Downsampler().calculateScaling('jpg', 1024, 1024, 200,
          200, DownsampleStrategy.AT_MOST)
      let req = (reqSize.targetWidth < 1024 && reqSize.targetHeight < 1024)
      expect(req).assertEqual(true);
    })
    it('FIT_CENTER', 2, () => {
      let reqSize: calculateScaleType =
        new Downsampler().calculateScaling('jpg', 1024, 1024, 200,
          200, DownsampleStrategy.FIT_CENTER_MEMORY)
      let req = (reqSize.targetWidth < 1024 && reqSize.targetHeight < 1024)
      expect(req).assertEqual(true);
    })
    it('CENTER_OUTSIDE', 3, () => {
      let reqSize: calculateScaleType =
        new Downsampler().calculateScaling('jpg', 1024, 1024, 200,
          200, DownsampleStrategy.CENTER_OUTSIDE_MEMORY)
      let req = (reqSize.targetWidth < 1024 && reqSize.targetHeight < 1024)
      expect(req).assertEqual(true);
    })
    it('AT_LEAST', 4, () => {
      let reqSize: calculateScaleType =
        new Downsampler().calculateScaling('jpg', 1024, 1024, 200,
          200, DownsampleStrategy.AT_LEAST)
      let req = (reqSize.targetWidth < 1024 && reqSize.targetHeight < 1024)
      expect(req).assertEqual(true);
    })

  })
}
