/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from '@ohos/hypium'
import {
  BlurTransformation,
  BrightnessTransformation,
  CropCircleTransformation,
  CropCircleWithBorderTransformation,
  CropSquareTransformation,
  CropTransformation,
  GrayScaleTransformation,
  InvertTransformation,
  KuwaharaTransformation,
  MaskTransformation,
  PixelationTransformation,
  SepiaTransformation,
  SketchTransformation,
  SwirlTransformation,
  ToonTransformation,
  VignetterTransformation
} from '@ohos/imageknife'

const BASE_COUNT: number = 1000;

export default function Transform() {
  describe('Transform', () => {
    // Defines a test suite. Two parameters are supported: test suite name and test suite function.
    beforeAll(() => {
      // Presets an action, which is performed only once before all test cases of the test suite start.
      // This API supports only one parameter: preset action function.

    })
    beforeEach(() => {
      // Presets an action, which is performed before each unit test case starts.
      // The number of execution times is the same as the number of test cases defined by **it**.
      // This API supports only one parameter: preset action function.
    })
    afterEach(() => {
      // Presets a clear action, which is performed after each unit test case ends.
      // The number of execution times is the same as the number of test cases defined by **it**.
      // This API supports only one parameter: clear action function.
    })
    afterAll(() => {
      // Presets a clear action, which is performed after all test cases of the test suite end.
      // This API supports only one parameter: clear action function.
    })
    it('TestBlurTransformation', 0, () => {
      let startTime = new Date().getTime();
      for (let index = 0; index < BASE_COUNT; index++) {
        new BlurTransformation(15);
      }
      endTime(startTime, 'TestBlurTransformation');
      let blur = new BlurTransformation(15);
      expect(blur.getName()).assertEqual('BlurTransformation;radius:15');
    })
    it('TestBrightnessTransformation', 1, () => {
      let startTime = new Date().getTime();
      for (let index = 0; index < BASE_COUNT; index++) {
        new BrightnessTransformation(20);
      }
      endTime(startTime, 'BrightnessTransformation');
      let bright = new BrightnessTransformation(20);
      expect(bright.getName()).assertEqual("BrightnessTransformation;bright:20");
    })
    it('TestCropCircleTransformation', 3, () => {
      let startTime = new Date().getTime();
      for (let index = 0; index < BASE_COUNT; index++) {
        new CropCircleTransformation();
      }
      endTime(startTime, 'TestCropCircleTransformation');
      let cropCircle = new CropCircleTransformation();
      expect(cropCircle.getName()).assertContain("CropCircleTransformation");
      expect(cropCircle.getName()).assertContain(";mCenterX:");
      expect(cropCircle.getName()).assertContain(";mCenterY:");
      expect(cropCircle.getName()).assertContain(";mRadius:");
    })
    it('TestCropCircleWithBorderTransformation', 4, () => {
      let startTime = new Date().getTime();
      for (let index = 0; index < BASE_COUNT; index++) {
        new CropCircleWithBorderTransformation(10, {
          r_color: 100, g_color: 100, b_color: 100
        });
      }
      endTime(startTime, 'TestCropCircleWithBorderTransformation');
      let CropCircleWithBorder = new CropCircleWithBorderTransformation(10, {
        r_color: 100, g_color: 100, b_color: 100
      });
      expect(CropCircleWithBorder.getName()).assertContain("CropCircleWithBorderTransformation")
      expect(CropCircleWithBorder.getName()).assertContain(";mCenterX:");
      expect(CropCircleWithBorder.getName()).assertContain(";mCenterY:");
      expect(CropCircleWithBorder.getName()).assertContain(";mRadius:");
      expect(CropCircleWithBorder.getName()).assertContain(";mBorderSize:");
      expect(CropCircleWithBorder.getName()).assertContain(";mRColor:");
      expect(CropCircleWithBorder.getName()).assertContain(";mGColor:");
      expect(CropCircleWithBorder.getName()).assertContain(";mBColor:");
    })
    it('TestCropSquareTransformation', 5, () => {
      let startTime = new Date().getTime();
      for (let index = 0; index < BASE_COUNT; index++) {
        new CropSquareTransformation();
      }
      endTime(startTime, 'TestCropSquareTransformation');
      let CropSquare = new CropSquareTransformation();
      expect(CropSquare.getName()).assertContain("CropSquareTransformation");
    })
    it('TestCropTransformation', 6, () => {
      let startTime = new Date().getTime();
      for (let index = 0; index < BASE_COUNT; index++) {
        new CropTransformation(10, 10, 1);
      }
      endTime(startTime, 'TestCropTransformation');
      let crop = new CropTransformation(10, 10, 1);
      expect(crop.getName()).assertContain("CropTransformation" + ";mWidth:10" + ";mHeight:10" + ";mCropType:1")
    })
    it('TestGrayScaleTransformation', 7, () => {
      let startTime = new Date().getTime();
      for (let index = 0; index < BASE_COUNT; index++) {
        new GrayScaleTransformation();
      }
      endTime(startTime, 'GrayScaleTransformation');
      let grayscale = new GrayScaleTransformation();
      expect(grayscale.getName()).assertContain("GrayScaleTransformation")
    })
    it('TestInvertTransformation', 8, () => {
      let startTime = new Date().getTime();
      for (let index = 0; index < BASE_COUNT; index++) {
        new InvertTransformation();
      }
      endTime(startTime, 'TestInvertFilterTransformation');
      let invert = new InvertTransformation();
      expect(invert.getName()).assertContain("InvertTransformation");
    })
    it('TestPixelationTransformation', 9, () => {
      let startTime = new Date().getTime();
      for (let index = 0; index < BASE_COUNT; index++) {
        new PixelationTransformation();
      }
      endTime(startTime, 'TestPixelationTransformation');
      let pixelation = new PixelationTransformation();
      expect(pixelation.getName()).assertContain("PixelationTransformation");
    })
    it('TestSepiaTransformation', 12, () => {
      let startTime = new Date().getTime();
      for (let index = 0; index < BASE_COUNT; index++) {
        new SepiaTransformation();
      }
      endTime(startTime, 'SepiaTransformation');
      let speia = new SepiaTransformation();
      expect(speia.getName()).assertContain("SepiaTransformation");
    })
    it('TestSketchTransformation', 13, () => {
      let startTime = new Date().getTime();
      for (let index = 0; index < BASE_COUNT; index++) {
        new SketchTransformation();
      }
      endTime(startTime, 'TestSketchTransformation');
      let Sketch = new SketchTransformation();
      expect(Sketch.getName()).assertContain("SketchTransformation");
    })
    it('TestMaskTransformation', 14, () => {
      let startTime = new Date().getTime();
      for (let index = 0; index < BASE_COUNT; index++) {
        new MaskTransformation($r('app.media.icon'));
      }
      endTime(startTime, 'TestMaskTransformation');
      let mask = new MaskTransformation($r('app.media.icon'));
      expect(mask.getName()).assertContain("MaskTransformation");
    })
    it('TestSwirlTransformation', 15, () => {
      let startTime = new Date().getTime();
      for (let index = 0; index < BASE_COUNT; index++) {
        new SwirlTransformation(10, 180, [10, 10]);
      }
      endTime(startTime, 'TestSwirlTransformation');
      let swirl = new SwirlTransformation(10, 180, [10, 10]);
      expect(swirl.getName()).assertContain("SwirlTransformation");
    })
    it('TestKuwaharaTransformation', 16, () => {
      let startTime = new Date().getTime();
      for (let index = 0; index < BASE_COUNT; index++) {
        new KuwaharaTransformation(10);
      }
      endTime(startTime, 'TestKuwaharaTransformation');
      let kuwahara = new KuwaharaTransformation(10);
      expect(kuwahara.getName()).assertContain("KuwaharaTransformation;radius:10");
    })
    it('TestToonTransformation', 17, () => {
      let startTime = new Date().getTime();
      for (let index = 0; index < BASE_COUNT; index++) {
        new ToonTransformation(10);
      }
      endTime(startTime, 'TestToonTransformation');
      let toon = new ToonTransformation(10);
      expect(toon.getName()).assertContain("ToonTransformation;threshold:10");
    })
    it('TestVignetterTransformation', 18, () => {
      let startTime = new Date().getTime();
      for (let index = 0; index < BASE_COUNT; index++) {
        new VignetterTransformation([0.5, 0.5], [0.0, 0.0, 0.0], [0.3, 0.75]);
      }
      endTime(startTime, 'TestVignetterTransformation');
      let vignette = new VignetterTransformation([0.5, 0.5], [0.0, 0.0, 0.0], [0.3, 0.75]);
      expect(vignette.getName()).assertContain("VignetterTransformation");
    })
  })
}

function endTime(startTime: number, tag: string) {
  let endTime: number = new Date().getTime();
  let averageTime = ((endTime - startTime) * 1000 / BASE_COUNT);
  console.info(tag + " startTime: " + endTime);
  console.info(tag + " endTime: " + endTime);
  console.log(tag + " averageTime: " + averageTime + "μs");
}