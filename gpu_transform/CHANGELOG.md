## 1.0.4
- 修改门禁编译问题 修改点如下
- 修改src/main/cpp/boundscheck/CMakeLists.txt文件中的内容
- 修改src/main/cpp/util/DebugLog.h文件中hilog的大小写

## 1.0.3
- 安全编译开启Strip和Ftrapv

## 1.0.2
- 支持x86编译

## 1.0.1
- 更新libc++sheared.so版本

## 1.0.0
- 包管理工具由npm切换为ohpm
- 适配DevEco Studio: 3.1Release(3.1.3.400)
- 适配SDK: API9 Release(3.2.11.5)

## 0.1.0
获取图片的buffer数据，使用openGL、着色器（Shader），操作GPU，达到图片滤波器的效果
- 支持模糊滤波器
- 支持亮度滤波器
- 支持颜色反转滤波器
- 支持对比度滤波器
- 支持灰色滤波器
- 支持桑原滤波器
- 支持马赛克滤波器
- 支持乌墨色滤波器
- 支持素描滤波器
- 支持扭曲滤波器
- 支持动画滤波器
- 支持装饰滤波器


